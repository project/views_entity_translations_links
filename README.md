# Views entity translations links

Provides an additional field with quick links that can be added in the
content listing and allows content managers to edit existing translations
or add missing ones much easier. Also gives an overview of the status of
translations for all pages directly in the Content view. The workflow is
simplified as it reduces the steps needed to perform these actions.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/views_entity_translations_links).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/views_entity_translations_links).


## Requirements

This module requires the following module:

- [Configuration Rewrite](https://www.drupal.org/project/config_rewrite)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## How to use

After enabling the module, the "Entity translations field" should be available
in the Display edit of the Content view: `/admin/structure/views/view/content`.

After you add the field a column with all enabled languages and the
translation status of each page should appear in the content listing.


## Configuration

This module does not have configuration.


## Maintainers

- Darius Restivan - [darius.restivan](https://www.drupal.org/u/dariusrestivan)
- Joeri van Dooren - [jvandooren](https://www.drupal.org/u/jvandooren)
